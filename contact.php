<?php $pageSlug = "contact"; ?>
<?php $pageTitle = "Contact Us"; ?>

<?php include('header.php'); ?>

<h2>Phone</h2>

<p>(925) 695-7782</p>

<h2>Email</h2>

<p><strong>Note: NOT FOR TECHNICAL QUESTIONS</strong><br>Send technical questions to the Slurm_dev mailing list <a href="http://slurm.schedmd.com/mail.html">here</a>.</p>

<form class="form" action="javascript:send_mail();" accept-charset="utf-8" method="post">

	<div class="form__field">
		<label class="form__label" for="contact_subject">Subject</label>
		<select id="contact_subject">
			<option value="sales">Sales</option>
			<option value="job">Career Opportunity</option>
			<option value="training">Training</option>
		</select>
	</div>

	<div class="form__field">
		<label class="form__label" for="contact_name">Name</label>
		<input class="form__input" type="text" size="25" id="contact_name">
	</div>

	<div class="form__field">
		<label class="form__label" for="contact_email">Email</label>
		<input class="form__input" type="email" size="25" id="contact_email">
	</div>

	<div class="form__field">
		<label class="form__label" for="contact_message">Message</label>
		<textarea class="form__input" rows="10" id="contact_message"></textarea>
	</div>

	<div class="form__field">
		<input type="checkbox" id="contact_cc" value="1"> <label class="form__label form__label--inline" for="contact_cc">Send CC to self</label>
	</div>

	<div class="form_field">
		<input class="form__submit button" type="submit" name="submit" value=" Send Email ">
	</div>

</form>



<?php include('footer.php'); ?>
