<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width" />

	<title>SchedMD | Slurm Support and Development</title>

	<link rel="shortcut icon" href="favicon.ico" />

	<link href="https://fonts.googleapis.com/css?family=Source+Code+Pro|Source+Sans+Pro:400,400i,600,600i|Ubuntu:300,300i,400,400i,500,500i" rel="stylesheet">
	<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	<link rel="stylesheet" type="text/css" href="/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
	<link rel="stylesheet" type="text/css" href="/slurm/css/slurm.css" />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="/js/site.js"></script>
	<script src="http://www.google.com/jsapi" type="text/javascript"></script>
	<script type="text/javascript">
	  google.load('search', '1', {language : 'en', style : google.loader.themes.V2_DEFAULT});
	  google.setOnLoadCallback(function() {
	    var customSearchOptions = {};  var customSearchControl = new google.search.CustomSearchControl(
	      '011890816164765777536:jvrtxrd3f0w', customSearchOptions);
	    customSearchControl.setResultSetSize(google.search.Search.SMALL_RESULTSET);
	    customSearchControl.draw('cse');
	  }, true);

	function window_check ()
	{
		obj = document.getElementById('navigation');
		if ((window.innerWidth>=800) && (window.innerHeight>=700) &&
		    (screen.width>=800) && (screen.height>=700)) {
		   obj.style.position = 'fixed';
		} else {
		   obj.style.position = 'absolute';
		}
	}

	window.onload = window_check;
	window.onresize = window_check;

	</script>
</head>

<body>

<div class="container container--main">

	<header class="site-header" role="banner">

		<div class="site-masthead">
			<h1 class="site-masthead__title site-masthead__title--slurm">
				<a href="/slurm" rel="home">
					<span class="slurm-logo">Slurm Workload Manager</span>
				</a>
			</h1>
			<div class="site-masthead__title">
				<a href="/" rel="home">
					<span class="site-logo">SchedMD</span>
				</a>
			</div>

			<button class="site-masthead__trigger menu-trigger" type="button" role="button" aria-label="Toggle Navigation"><span class="menu-trigger__lines"></span></button>
		</div>


		<nav class="site-nav">
			<h2 class="site-nav__title">Navigation</h2>

			<div class="slurm-title">
				<div class="slurm-logo"><a href="/slurm">Slurm Workload Manager</a></div>
				<div class="slurm-title__version">Version 16.05</div>
			</div>
		
			<ul class="site-nav__menu slurm-menu menu" role="navigation">
				<li class="site-menu__item slurm-menu__item">
					About
					<ul class="slurm-menu__sub">
						<li><a href="overview.shtml" class="nav">Overview</a></li>
						<li><a href="news.shtml" class="nav">What's New</a></li>
						<li><a href="team.shtml" class="nav">Slurm Team</a></li>
						<li><a href="meetings.shtml" class="nav">Meetings</a></li>
						<li><a href="testimonials.shtml" class="nav">Testimonials</a></li>
						<li><a href="disclaimer.shtml" class="nav">Legal Notices</a></li>
					</ul>
				</li>
				<li class="site-menu__item slurm-menu__item">
					Using
					<ul class="slurm-menu__sub">
						<li><a href="tutorials.shtml" class="nav">Tutorials</a></li>
						<li><a href="documentation.shtml" class="nav">Documentation</a></li>
						<li><a href="faq.shtml" class="nav">FAQ</a></li>
						<li><a href="publications.shtml" class="nav">Publications</a></li>
					</ul>	
				</li>
				<li class="site-menu__item slurm-menu__item">
					Installing
					<ul class="slurm-menu__sub">
						<li><a href="download.shtml" class="nav">Download</a></li>
						<li><a href="quickstart_admin.shtml" class="nav">Installation Guide</a></li>
					</ul>
				</li>
				<li class="site-menu__item slurm-menu__item">
					Getting Help
					<ul class="slurm-menu__sub">
						<li><a href="http://www.schedmd.com/#services" class="nav">Support</a></li>
						<li><a href="mail.shtml" class="nav">Mailing Lists</a></li>
						<li><a href="http://www.schedmd.com/#services" class="nav">Training</a></li>
						<li><a href="troubleshoot.shtml" class="nav">Troubleshooting</a></li>
					</ul>
				</li>
			</ul>

		</nav>

	</header>

	<div class="content" role="main">
		<section class="slurm-search">
			<div class="container">
				<div id="cse"></div>
			</div>
		</section>

		<div class="section">
			<div class="container">
