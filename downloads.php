<?php $pageSlug = "downloads"; ?>
<?php $pageTitle = "Downloads"; ?>

<?php include('header.php'); ?>

<section class="page-section">
	<h2>Download the latest stable version of Slurm</h2>

	<p><a href="javascript:handle_download('download/latest/slurm-15.08.12.tar.bz2');" class="button">slurm-15.08.12.tar.bz2</a> <br> md5: <b>33c30666e77854f05104016814f5f315</b></p>

	<p><a href="javascript:handle_download('download/latest/slurm-16.05.3.tar.bz2');" class="button">slurm-16.05.3.tar.bz2</a> <br> md5: <b>4142053ab00ed3d0aaaf22eff314bd8b</b></p>
</section>

<section class="page-section">
	<h2>Download the latest development version of Slurm</h2>

	<p><a href="javascript:handle_download('download/development/slurm-17.02.0-0pre1.tar.bz2');" class="button">slurm-17.02.0-0pre1.tar.bz2</a> <br> md5: <b>fd42f53c4f3e2fcb82b706efcb64783a</b></p>
</section>

<section class="page-section">
	<h2>Get Commercial Support</h2>

	<p>You can obtain the latest bleeding-edge SchedMD source code by anonymous Git access.</p>

	<pre><code>git clone git://github.com/SchedMD/slurm.git</code></pre>

	<p>Note: For more information about using Git, including tutorials and guides to help you get started, see the <a href="http://git-scm.com/documentation" target="_blank" class="general">Git documentation page</a>.</p>

	<p>If you have questions or experience problems, please <a href="javascript:change_view('contact');" class="general">contact us</a>.<br></p>
</section>

<section class="page-section">
	<h2>License</h2>

	<p><a href="http://www.gnu.org/licenses/gpl-2.0.html">GNU General Public License (GPL) V2</a></p>
</section>

<div class="page-section">
	<p><a class="button" href="javascript:change_view('archives');">Older Versions</a></p>
</div>

<?php include('footer.php'); ?>