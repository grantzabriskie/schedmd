jQuery(document).ready(function() {
	jQuery('.menu-trigger').bind('click touchstart', function() {
		jQuery(this).find('.menu-trigger__lines').toggleClass('menu-trigger__lines--closed');
		jQuery(this).parents('.site-header').find('.site-nav').toggleClass('site-nav--active');

		return false;
	});
});