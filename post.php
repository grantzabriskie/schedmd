<?php $pageSlug = "post"; ?>
<?php $pageTitle = "Slurm 16.05.3 and 17.02.0-pre1 are now available"; ?>
<?php $post = true; ?>

<?php include('header.php'); ?>

<p class="news-post__meta">SchedMD News Release: Jul 26, 2016</p>

	<p>Slurm version 16.05.3 is now available and includes about 30 bug fixes developed over the past few weeks. We have also released the first pre-release of version 17.02, which is under development and scheduled for release in February 2017. A description of the changes in each version is appended.</p>

	<p>Slurm downloads are available from here.</p>

</article>

<?php include('footer.php'); ?>
