<?php $pageSlug = "services"; ?>
<?php $pageTitle = "Services"; ?>

<?php include('header.php'); ?>

<p>SchedMD offers a full line of services for the Slurm Workload Manager. Services include</p>
<ol>
	<li>Support agreements for Slurm Clusters</li>
	<li>Custom Slurm development</li>
	<li>Configuration assistance</li>
	<li>Training</li>
	<li>Customization</li>
</ol>

<p>Please use our <a href="http://schedmd.com/#contact">contact us</a> form or email <a href="mailto:sales@schedmd.com?Subject=SchedMD%20Professional%20Services">sales@schedmd.com</a>  questions regarding the professional services offered by SchedMD.</p>

<p><a class="button" href="http://schedmd.com/#contact">Contact Us</a></p>

<h2>About Slurm</h2>

<p>Slurm is open source software, but it is also a critical component of the computer's operation. As the Slurm experts, SchedMD is the company that most organizations rely upon to keep their systems running. SchedMD customers include national laboratories, universities and computer vendors world-wide, a few of which are listed below.</p>

<ol>
	<li>Brigham Young University</li>
	<li>Bull</li>
	<li>CEA</li>
	<li>Cray</li>
	<li>Harvard University</li>
	<li>Lawrence Livermore National Laboratory</li>
	<li>Meteo France</li>
	<li>MIT</li>
	<li>NASA Center for Climate Simulation</li>
	<li>Swiss National Supercomputing Centre</li>
</ol>

<p>Our general provisions are listed <a href="download/extras/SchedMD_General_Provisions.pdf">here</a>.</p>

<?php include('footer.php'); ?>
