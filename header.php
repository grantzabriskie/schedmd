<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width" />

	<?php if ($pageSlug == 'home') { ?>
	<title>SchedMD | Slurm Support and Development</title>
	<?php } else { ?>
	<title><?php echo $pageTitle; ?> | SchedMD</title>
	<?php } ?>

	<link rel="shortcut icon" href="favicon.ico" />

	<link href="https://fonts.googleapis.com/css?family=Source+Code+Pro|Source+Sans+Pro:400,400i,600,600i|Ubuntu:300,300i,400,400i,500,500i" rel="stylesheet">
	<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	<link rel="stylesheet" type="text/css" href="/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="/css/style.css" />

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="/js/site.js"></script>
</head>

<body>

<div class="container container--main">

	<header class="site-header" role="banner">

		<div class="site-masthead">
			<h1 class="site-masthead__title site-title">
				<a href="/" rel="home">
					<span class="site-logo">SchedMD</span>
					<span class="site-title__slogan">Slurm Support and Development</span>
				</a>
			</h1>

			<button class="site-masthead__trigger menu-trigger" type="button" role="button" aria-label="Toggle Navigation"><span class="menu-trigger__lines"></span></button>
		</div>

		<nav class="site-nav">
			<h2 class="site-nav__title">Navigation</h2>
		
			<ul class="site-nav__menu site-menu menu" role="navigation">
				<li class="site-menu__item<?php if ($pageSlug == 'home') { echo ' site-menu__item--current'; } ?>"><a href="/"><span>Home</span></a></li>
				<li class="site-menu__item<?php if ($pageSlug == 'services') { echo ' site-menu__item--current'; } ?>"><a href="/services.php"><span>Services</span></a></li>
				<li class="site-menu__item<?php if ($pageSlug == 'downloads') { echo ' site-menu__item--current'; } ?>"><a href="/downloads.php"><span>Downloads</span></a></li>
				<li class="site-menu__item<?php if ($pageSlug == 'news') { echo ' site-menu__item--current'; } ?>"><a href="/news.php"><span>News</span></a></li>
				<li class="site-menu__item<?php if ($pageSlug == 'events') { echo ' site-menu__item--current'; } ?>"><a href="/events.php"><span>Events</span></a></li>
				<li class="site-menu__item<?php if ($pageSlug == 'contact') { echo ' site-menu__item--current'; } ?>"><a href="/contact.php"><span>Contact</span></a></li>
			</ul>

			<section class="site-nav__features" role="complementary">
				<article class="featured__item feature">
					<h2 class="feature__title">Latest News</h2>

					<p><a href="">Slurm 16.05.0 and 15.08.12 are now available</a></p>

					<p><a href="">Slurm version 16.05.0-rc2 available</a></p>
				</article>

				<article class="featured__item feature">
					<h2 class="feature__title">Documentation</h2>

					<p>Learn more about Slurm by reading the <a href="">Slurm Documenation</a>.</p>
				</article>

				<article class="featured__item feature">
					<h2 class="feature__title">Downloads</h2>

					<p>Visit our <a hred="">downloads section</a> to get the latest version of Slurm.</p>
				</article>

				<article class="featured__item feature">
					<h2 class="feature__title">Get Involved</h2>

					<p>Get involved in the development of Slurm by joining our <a href="">mailing list</a>. For those only interested in Slurm announcements, join our <a href="">Slurm Announce sublist</a>. Be sure to follow us on <a href="">LinkedIn</a>, <a href="">Facebook</a>, and <a href="">other social sites</a>.</p>
				</article>
			</section>

		</nav>

	</header>

	<div class="content section" role="main">
		<div class="container">

			<h1 class="page-title page-title--major<?php if ($post == true) { echo ' news-post__title'; } ?>"><?php echo $pageTitle; ?></h1>
