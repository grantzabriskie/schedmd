		</div>
	</div> <!-- END .content -->
</div> <!-- END .main -->


<aside class="highlight section">
	<div class="container">

		<p class="highlight__text">Slurm is the workload manger on the most powerful computer in the world, Tianhe-2, with 3.1 million cores and a peak performance of 33.86 petaflops.</p>

	</div>
</aside>

<footer class="site-footer" role="contentinfo">

	<nav class="footer-nav section">
		<div class="container">
			<ul class="footer-nav__menu footer-menu menu" role="navigation">
				<li class="footer-menu__item"><a href="/">Home</a></li>
				<li class="footer-menu__item"><a href="/services.php">Services</a></li>
				<li class="footer-menu__item"><a href="/downloads.php">Downloads</a></li>
				<li class="footer-menu__item"><a href="/news.php">News</a></li>
				<li class="footer-menu__item"><a href="/events.php">Events</a></li>
				<li class="footer-menu__item"><a href="/careers.php">Careers</a></li>
				<li class="footer-menu__item"><a href="https://bugs.schedmd.com/">Bugs</a></li>
				<li class="footer-menu__item"><a href="/contact.php">Contact</a></li>
			</ul>

			<ul class="footer-nav__social social-menu menu">
				<li class="social-menu__item"><a href=""><i class="ion-social-facebook"></i> <span>Facebook</span></a></li>
				<li class="social-menu__item"><a href=""><i class="ion-social-twitter"></i> <span>Twitter</span></a></li>
				<li class="social-menu__item"><a href=""><i class="ion-social-linkedin"></i> <span>LinkedIn</span></a></li>
				<li class="social-menu__item"><a href=""><i class="ion-social-googleplus"></i> <span>Google+</span></a></li>
			</ul>
		</div>
	</nav>

	<section class="footer-copyright">
		<div class="container">
			<p>© 2011-2016 SchedMD LLC. All rights reserved.</p>
		</div>
	</section>

</footer>


</body>
</html>