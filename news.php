<?php $pageSlug = "news"; ?>
<?php $pageTitle = "News"; ?>

<?php include('header.php'); ?>

<article class="news-post" onclick="change_view('169');">

	<h2 class="news-post__title">Slurm 16.05.3 and 17.02.0-pre1 are now available</h2>

	<p class="news-post__meta">SchedMD News Release: Jul 26, 2016</p>

	<p>Slurm version 16.05.3 is now available and includes about 30 bug fixes developed over the past few weeks. We have also released the first pre-release of version 17.02, which is under development and scheduled for release in February 2017. A description of the changes in each version is appended. ... </p>

	<p class="news-post__readmore">Read more</p>

</article>

<article class="news-post" onclick="change_view('168');">

	<h2 class="news-post__title">Slurm versions 16.06.3 and 17.02.0-pre1 are now available</h2>
	
	<p class="news-post__meta">SchedMD News Release: Jul 26, 2016</p>

	<p>Slurm version 16.05.3 is now available and includes about 30 bug fixes developed over the past few weeks. We have also relesed the first pre-release of version 17.02, which is under development and scheduled for release in February 2017. Slurm downloads are available from ...</p>

	<p class="news-post__readmore">Read more</p>

</article>

<article class="news-post" onclick="change_view('168');">

	<h2 class="news-post__title">Slurm version 16.05.2 is now available</h2>
	
	<p class="news-post__meta">SchedMD News Release: Jul 6, 2016</p>

	<p>Slurm version 16.05.2 is now available and includes 16 bug fixes developed over the past week, including two which can cause the slurmctld daemon to crash. Slurm downloads are available from http://www.schedmd.com/#repos.</p>

	<p class="news-post__readmore">Read more</p>

</article>

<?php include('footer.php'); ?>
