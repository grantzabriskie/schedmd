<?php $pageSlug = "home"; ?>
<?php $pageTitle = "Slurm Commerical Support and Development"; ?>

<?php include('header.php'); ?>

<p>SchedMD is the core company behind the Slurm workload manager software, a free open-source workload manager designed specifically to satisfy the demanding needs of high performance computing. Slurm is in widespread use at government laboratories, universities and companies world wide. As of the June 2016 Top 500 computer list, Slurm was performing workload management on five of the ten most powerful computers in the world including the number 2 system, Tianhe-2 with 3,120,000 computing cores.
</p>

<p>SchedMD performs most Slurm development, reviews and integrates contributions from others, distributes and maintains the canonical version of Slurm. SchedMD also provides <a href="http://schedmd.com/#services" class="general">training</a>, <a href="http://schedmd.com/#services" class="general">installation</a>, <a href="http://schedmd.com/#services" class="general">configuration</a>, <a href="http://schedmd.com/#services" class="general">custom development</a> and <a href="http://schedmd.com/#services" class="general">support</a> (including joint Slurm support with Bull, Cray and Science+Computing).</p>

<p>Slurm is a highly configurable open-source workload manager. In its simplest configuration, it can be installed and configured in a few minutes (see <a href="http://www.linux-mag.com/id/7239/1/" target="_blank" class="general">Caos NSA and Perceus: All-in-one Cluster Software Stack</a> by Jeffrey B. Layton). Use of optional plugins provides the functionality needed to satisfy the needs of demanding HPC centers. More complex configurations rely upon a database for archiving accounting records, managing resource limits by user or bank account, and supporting sophisticated scheduling algorithms.</p>

<p>Meet the founders of SchedMD LLC, Morris Jette and Danny Auble: <a href="http://www.rce-cast.com/index.php/Podcast/rce-10-slurm.html" target="_blank" class="general">Research Computing &amp; Engineering, Slurm Podcast</a></p>

<p><a class="button" href="javascript:handle_download('slurmdocs/pdfs/schedmd_slurm_data.pdf');" class="general">SchedMD/Slurm Data Sheet</a></p>


<?php include('footer.php'); ?>
