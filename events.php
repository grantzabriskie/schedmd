<?php $pageSlug = "events"; ?>
<?php $pageTitle = "Events"; ?>

<?php include('header.php'); ?>

<article class="news-post" onclick="change_view('169');">

	<h2 class="news-post__title">Slurm User Group Meeting (SLUG) 2016</h2>

	<p class="news-post__meta">SchedMD News Release: Jul 26, 2016</p>

	<p>This event is sponsored and organized by Greek Research and Technology Network (GRNET) and SchedMD.</p> 

	<p>It will be held at the Technopolis, 100 Pireos Street in Athens, Greece on 26-27 September 2016.</p>

	<p>This international event is opened to everyone who wants to:</p>

	<p>- Learn</p>

</article>

<?php include('footer.php'); ?>
