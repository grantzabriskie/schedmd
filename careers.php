<?php $pageTitle = "Careers at SchedMD"; ?>

<?php include('header.php'); ?>

<p>We invite experienced Linux systems programmers and test engineers looking for an exciting opportunity in a challenging and rewarding environment to <a href="javascript:change_view('contact');" class="general">contact us</a>. Experience with Slurm or other job scheduling software in a high performance computing environment is strongly preferred.</p>

<p>Experienced Slurm developers interested in consulting work are also invited to <a href="javascript:change_view('contact');" class="general">contact us</a>.</p>

<h2>Location</h2>

<p>SchedMD is based in Livermore, California, near San Francisco and the Silicon Valley. An outstanding climate provides ample opportunity to enjoy a wide range of recreation activities year round.</p>

<h2>About SchedMD</h2>

<p>SchedMD was founded in 2010 by the primary developers of Slurm to provide development and support for job scheduling in the high-performance computing market. Our customer base is concentrated in government laboratories and universities world wide. </p>

<p><a class="button" href="javascript:handle_download('slurmdocs/pdfs/schedmd_slurm_data.pdf');" class="general">SchedMD/Slurm Data Sheet</a></p>

<?php include('footer.php'); ?>
